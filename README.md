# wine-price-calc
Brainteaser to play around with Ruby and TDD! The question is available [here](https://gitlab.com/avosa/wine-price-calc/-/blob/main/spec/task.txt).

## Getting started
1. Navigate to the directory
```
cd ~/wine-price-calc
```

2. Install dependancies
```
bundle install
```
3. Then run
```
rspec -fd
```

Enjoy! 

# Author

[Webster Avosa](https://github.com/avosa)
