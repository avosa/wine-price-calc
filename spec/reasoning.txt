PLANNING/REASONING

We want to instantiate the base_price and tiers. Our goal is to have tiers parsed
in as an array of hashes in this form:
    [
        {from: value, to: value, rate: value},
        {from: value, to: value, rate: value},
        {etc }
    ]; Where (from) and (to) represent the range of the prices and (rate) represents
the percentage i.e. {from: value, to: value, rate: value}.

We want to use float for prices so that where the range is above a given value,
say above 50, our hash will be, {from: 50, to: Float::INFINITY, rate: 1.34}.Then the
calculate_price method will do the calculation based on the base_price and indexes of
the custom tiers parsed as illustrated below:
    STEP 1:
        (i.)If the base price is equal to or less than a difference between to and from,
            this method should evaluate base price * rate and break. For example, assuming
            our base price 13 is equal to or less than the difference between to and from for
            tier[0], our result should be 13 * rate + 13. For this case, we are assuming tier[0]
            is {from: 0, to: 0} so yes it does! Then break because we don't have any other business
            to do here.
        (ii.)If the base price is greater than the difference
            between to and from, evaluate only the condition that is true then the remaining
            bit(the greater bit/excess/extra bit) is pushed to step 2.

    STEP 2:
        If the number is greater than the difference between to and from, from STEP 1 above,
        it should be evaluated still, then, the base price is deducted from (the difference
        between to and from). For example, assuming our base price is 20 and is greater than the
        difference between to and from for tier[0], we expect this method to compute (the result
        of STEP 1 + STEP 2's base case of value 10(which in this case will now be a result of subtracting
        tier[0]'s (difference between to and from which for this case is 15) from the original base case
        if and only if 10 lies under the margin of tier[1]'s margin i.e. the difference between to and
        from(For this case we are assuming tier[1] is{from: 15, to: 25} so yes it does!) * rate. Break if
        the condition is true. If the base price is greater than the difference between to and from,
        evaluate only the condition that is true then the remaining bit(the greater bit/excess/extra bit)
        is pushed to step 3.

    STEP 3
        If the number is greater than the difference between to and from, from STEP 2 above, it should be
        evaluated still, then, the base price is deducted from (the difference between to and from). For
        example, assuming our base price is 32 and is greater than the difference between to and from for
        tier[1], we expect this method to compute (the result of (ii) + STEP 3's base case of value 12(which
        in this case will now be a result of subtracting tier[2]'s (difference between to and from which for
        this case is 10 from our previous assumption values i.e. {from: 15, to: 25}) from the original base
        case if and only if 12 lies under the margin of tier[1]'s margin i.e. the difference between to and
        from(For this case we are assuming tier[2] is{from: 25, to: 35} so yes it does!) * rate. Break if
        the condition is true. If the base price is greater than the difference between to and from, evaluate
        only the condition that is true then the remaining bit(the greater bit/excess/extra bit) is pushed to
        step 4.

    STEP 4, 5, 6, INFINITY will still do as above(taking evaluation of immediately previous STEP + evaluation
        from it's on tier.

So now we could now do stuff like 👇🏾

    instance = Class.new(base_price, tiers_which_are_in_the_form_of_array_of_hashes)

    puts instance.calculate_method

For example:

    wine_price = WinePriceCalculator.new(
            300, [
                    {from: 0, to: 15, rate: 0.51},
                    {hash two}, {hash three},
                    {hash four},
                    { ….},
                    {hash infinity}
                ]
            )

    puts wine_price.calculate_price
