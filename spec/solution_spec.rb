class  WinePriceCalculator

  attr_reader :base_price, :tiers

  def initialize(base_price, tiers)
    @base_price = base_price
    @tiers = tiers
  end

  def calculate_price
    result = 0.00
    base_price_copy = base_price

    tiers.each do |tier|
      if base_price_copy <= tier[:to] - tier[:from]
        result += base_price_copy * tier[:rate]
        result += base_price_copy
        break
      elsif base_price_copy > tier[:to] - tier[:from]
        result += (tier[:to] - tier[:from]) * tier[:rate]
        result += (tier[:to] - tier[:from])
        base_price_copy -= (tier[:to] - tier[:from])
      end
    end

    result.round(2)
  end
end

# tiers = [
#   { from: 0, to: 15, rate: 0.51 },
#   { from: 15, to: 30,rate: 0.41 },
#   { from: 30, to: 50, rate: 0.31 },
#   { from: 50, to: Float::INFINITY, rate: 0.21 }
# ]


# p WinePriceCalculator.new(15, tiers).calculate_price
# p WinePriceCalculator.new(28, tiers).calculate_price
# p WinePriceCalculator.new(35, tiers).calculate_price
# p WinePriceCalculator.new(40, tiers).calculate_price
# p WinePriceCalculator.new(75, tiers).calculate_price

require "rspec"
RSpec.describe WinePriceCalculator do
  let(:wine) { described_class.new base_price, tiers}

  context "#calculate_price" do
    subject { wine.calculate_price }
    let(:tiers) {
      [
        { from: 0, to: 15, rate: 0.51 },
        { from: 15, to: 30,rate: 0.41 },
        { from: 30, to: 50, rate: 0.31 },
        { from: 50, to: Float::INFINITY, rate: 0.21 }
      ]
    }

    context "for base price <= 15" do
      let(:base_price) { 15 }

      it { is_expected.to eq 22.65 }
    end

    context "for base price between $15 and $30" do
      let(:base_price) { 28 }

      it { is_expected.to eq 40.98 }
    end

    context "for base price between $30 and $50" do
      let(:base_price) { 35 }

      it { is_expected.to eq 50.35 }
    end

    context "for base price over $50" do
      let(:base_price) { 75 }

      it { is_expected.to eq 100.25 }
    end
  end
end

