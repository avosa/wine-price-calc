# # MY REASONING
# # 1.  The method should take an input of a price(which can be a float or integar).
# # 2.  If the price is less than or equal to 15, the method returns
# #         a value of base_price * 1.51.
# # 3.  If the price is greater than 15, but less than or equal to 30,
# #         the method returns a value of (15 * 1.51) + ((base_price - 15) * 1.41).
# # 4.  If the price is greater than 30, but less than or equal to 50, the method
# #          returns a value of (15 * 1.51) + ((30 - 15) * 1.41) + ((base_price - 30) * 1.31).
# # 5.  If the price is greater than 50, the method returns a value of
# #         (15 * 1.51) + ((30 - 15) * 1.41) + ((50 - 30) * 1.31) + ((base_price - 50) * 1.21).

# class WinePriceCalculator
#   attr_reader :base_price

#   def initialize(base_price)
#     @base_price = base_price
#   end

#   def calculate_wine_price
#     if base_price <= 15
#       base_price * 1.51
#     elsif base_price > 15 && base_price <= 30
#       (15 * 1.51) + ((base_price - 15) * 1.41)
#     elsif base_price > 30 && base_price <= 50
#       (15 * 1.51) + ((30 - 15) * 1.41) + ((base_price - 30) * 1.31)
#     else
#       (15 * 1.51) + ((30 - 15) * 1.41) + ((50 - 30) * 1.31) + ((base_price - 50) * 1.21)
#     end
#   end
# end

# # wine = WinePriceCalculator.new 15
# # p wine.base_price
# # p wine.calculate_wine_price

# require "rspec"
# RSpec.describe WinePriceCalculator do
#   let(:wine) { WinePriceCalculator.new base_price }

#   context "#calculate_wine_price" do
#     subject { wine.calculate_wine_price }

#     context "for base price <= 15" do
#       let(:base_price) { 15 }

#       it { is_expected.to eq 22.65 }
#     end

#     context "for base price between $15 and $30" do
#       let(:base_price) { 28 }

#       it { is_expected.to eq 40.98 }
#     end

#     context "for base price between $30 and $50" do
#       let(:base_price) { 40 }

#       it { is_expected.to eq 56.90 }
#     end

#     context "for base price over $50" do
#       let(:base_price) { 75 }

#       it { is_expected.to eq 100.25 }
#     end
#   end
# end
